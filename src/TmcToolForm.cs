using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BizHawk.Client.Common;
using BizHawk.Client.EmuHawk;

namespace RngTool
{
    [ExternalTool("TMC Tool")]
    public sealed class TmcToolForm : ToolFormBase, IExternalToolForm
    {
        public ApiContainer? _maybeAPIContainer { get; set; }
        private ApiContainer APIs => _maybeAPIContainer!;

        protected override string WindowTitleStatic => "TMC Tool";

        private TabPage tabRomInfo;
        private Label romInfoHash;
        private CheckBox checkBoxOverwriteIsTmc;
        private TabControl tabControl;

        private bool isTmc;

        public TmcToolForm()
        {
            ClientSize = new Size(480, 320);
            SuspendLayout();

            tabControl = new TabControl();
            tabControl.Dock = DockStyle.Fill;
            Controls.Add(tabControl);

            tabRomInfo = new TabPage();
            tabRomInfo.Text = "Rom Info";
            tabControl.Controls.Add(tabRomInfo);

            romInfoHash = new Label();
            romInfoHash.AutoSize = true;
            tabRomInfo.Controls.Add(romInfoHash);

            checkBoxOverwriteIsTmc = new CheckBox();
            checkBoxOverwriteIsTmc.Text = "overwrite rom detection";

            ResumeLayout();
        }

        private string MemoryReadString(long addr, int length, string domain = null)
        {
            return System.Text.Encoding.ASCII.GetString(APIs.Memory.ReadByteRange(addr, length, domain).ToArray());
        }

        private static readonly Dictionary<string, string> GameCodes = new Dictionary<string, string>
        {
            { "BZHE", "USA (DEMO)" },
            { "BZME", "USA" },
            { "BZMP", "EU" },
            { "BZMJ", "JP" }
        };

        private static readonly Dictionary<string, string> GameHashes = new Dictionary<string, string>
        {
            { "B4BD50E4131B027C334547B4524E2DBBD4227130", "USA" },
            { "6C5404A1EFFB17F481F352181D0F1C61A2765C5D", "JP" },
            { "CFF199B36FF173FB6FAF152653D1BCCF87C26FB7", "EU" },
            { "63FCAD218F9047B6A9EDBB68C98BD0DEC322D7A1", "USA (DEMO)" },
            { "9CDB56FA79BBA13158B81925C1F3641251326412", "JP (DEMO)" },
            { "9800F27A317339DF67F3A3B580178D2F9CFBD895", "JP (Firerod Speedrun Hack)" },
        };

        private string GetRomInfoText()
        {
            var system = APIs.Emulation.GetSystemId();
            if(system != "GBA")
            {
                return "This tool only supports The Legend of Zelda - The Minish Cap for GBA";
            }
            var gameInfo = APIs.GameInfo.GetGameInfo()!;
            var gameCode = MemoryReadString(0xAC, 4, "ROM");
            if(!GameCodes.ContainsKey(gameCode))
            {
                return "This tool only supports The Legend of Zelda - The Minish Cap for GBA";
            }
            isTmc = true;
            var gameVersion = GameHashes.ContainsKey(gameInfo.Hash)
                ? GameHashes[gameInfo.Hash]
                : String.Format("Hack based on {0}", GameCodes[gameCode]);
            return String.Format("Version: {0}", gameVersion);
        }

        private bool IsTmc()
        {
            return isTmc || checkBoxOverwriteIsTmc.Checked;
        }
        
        public override void Restart()
        {
            isTmc = false;
            checkBoxOverwriteIsTmc.Checked = false;
            romInfoHash.Text = GetRomInfoText();
        }
    }
}